library ieee;
use ieee.std_logic_1164.all;

entity BCD2sevenSeg is
	port(
			i : in std_logic_vector(3 downto 0);
			--clk : in std_logic;
			o : out std_logic_vector(6 downto 0));
end BCD2sevenSeg;

architecture data_process of BCD2sevenSeg is
begin
	process(i)
		begin
			--if clk'event and clk='1' then
				case i is
					when "0000" => o <= "0000001";
					when "0001" => o <= "1001111";
					when "0010" => o <= "0010010";
					when "0011" => o <= "0000110";
					when "0100" => o <= "1001100";
					when "0101" => o <= "0100100";
					when "0110" => o <= "0100000";
					when "0111" => o <= "0001111";
					when "1000" => o <= "0000000";
					when "1001" => o <= "0000100";
					when others => o <= "ZZZZZZZ";
					
				end case;
	end process;
end data_process;	