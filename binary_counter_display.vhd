library ieee;
use ieee.std_logic_1164.all;
entity binary_counter_display is
	port( X, Y, CLR, CLK : in std_logic;
		binary_out, binary_ten_out: out std_logic_vector(6 downto 0));
end binary_counter_display;

architecture Structural of binary_counter_display is

component binary_counter is
	port(B0, B1, CLR, CLK : in std_logic;
		  Q , T : out std_logic_vector(3 downto 0));
end component;

component BCD2sevenSeg is
	port(i: in std_logic_vector(3 downto 0);
		  o  : out std_logic_vector(6 downto 0));
end component;

component binary_counter_display_ten is
	port(j: in std_logic_vector(3 downto 0);
	     o2: out std_logic_vector(6 downto 0));
end component;

signal a, b : std_logic_vector(3 downto 0);
	begin
		U0: binary_counter
			port map(X,Y,CLR,CLK,a,b);
		U1: BCD2sevenSeg
			port map(a,binary_out);
		U2: binary_counter_display_ten
		   port map(b,binary_ten_out);
end Structural;

