library verilog;
use verilog.vl_types.all;
entity binary_counter_display_vlg_check_tst is
    port(
        binary_out      : in     vl_logic_vector(6 downto 0);
        binary_ten_out  : in     vl_logic_vector(6 downto 0);
        sampler_rx      : in     vl_logic
    );
end binary_counter_display_vlg_check_tst;
