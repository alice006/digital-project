library verilog;
use verilog.vl_types.all;
entity binary_counter_display_vlg_sample_tst is
    port(
        CLK             : in     vl_logic;
        CLR             : in     vl_logic;
        X               : in     vl_logic;
        Y               : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end binary_counter_display_vlg_sample_tst;
