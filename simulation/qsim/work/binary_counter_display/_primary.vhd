library verilog;
use verilog.vl_types.all;
entity binary_counter_display is
    port(
        X               : in     vl_logic;
        Y               : in     vl_logic;
        CLR             : in     vl_logic;
        CLK             : in     vl_logic;
        binary_out      : out    vl_logic_vector(6 downto 0);
        binary_ten_out  : out    vl_logic_vector(6 downto 0)
    );
end binary_counter_display;
