library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity binary_counter is
	port(B0, B1, CLR, CLK : in std_logic;
			Q, T : out std_logic_vector(3 downto 0));
end binary_counter;

architecture Behavioral of binary_counter is
	signal tmp, tmpten : std_logic_vector(3 downto 0);
	signal count : integer;
	signal overflow, underflow, lowest, highest, types : std_logic := '0';
		begin
		 overflow <= '1' when (tmp = "1001") else '0';
		 underflow <= '1' when (tmp = "0000") else '0';
		 lowest <= '1' when (tmpten = "0000" and tmp = "0000") else '0';
		 highest <= '1' when (tmpten = "1001" and tmp = "1001") else '0';
			process(B0, B1, CLR, CLK)
			 begin
			  if rising_edge(CLK) then
			    if (CLR='0')then
			      tmp <= "0000";
					tmpten <= "0000";
				 elsif (B0='0' and highest='0') then
				   count <= count + 1;
					types <= '0';
				 elsif (B1='0' and lowest='0') then
				   count <= count + 1;
					types <= '1';
				 else
				   if (count>5)then
					  count <= 0;
					  if (types='0')then
					    if (overflow='1')then
				         tmp <= "0000";
				         tmpten <= tmpten + 1;
					    else
				         tmp <= tmp + 1;
					    end if;
					  elsif (types='1')then
					    if (underflow='1')then
				         tmp <= "1001";
			            tmpten <= tmpten - 1;
					    else
					      tmp <= tmp - 1;
					    end if;
					  end if;
				   elsif (count<=5)then
					  if(B1='1' and B0='1')then
					    count <= 0;
					  end if;
			    end if;
			 end if;
			 end if;
			 end process;
			 Q <= tmp;
			 T <= tmpten;
end Behavioral;