library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity binary_counter_display_ten is
	port( 
	      j : in std_logic_vector(3 downto 0);
			o2 : out std_logic_vector(6 downto 0));
end binary_counter_display_ten;

architecture data_process2 of binary_counter_display_ten is
begin
	process(j)
		begin
			  case j is
					when "0000" => o2 <= "0000001";
					when "0001" => o2 <= "1001111";
					when "0010" => o2 <= "0010010";
					when "0011" => o2 <= "0000110";
					when "0100" => o2 <= "1001100";
					when "0101" => o2 <= "0100100";
					when "0110" => o2 <= "0100000";
					when "0111" => o2 <= "0001111";
					when "1000" => o2 <= "0000000";
					when "1001" => o2 <= "0000100";
					when others => o2 <= "ZZZZZZZ";
				end case;
	end process;
end data_process2;